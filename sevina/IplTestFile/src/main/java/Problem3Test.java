import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class Problem3Test  {

    @Test
    public void Problem2Test(){
        Problem3 obj = new Problem3();
        ReadMatchData matchData = new ReadMatchData();
        ReadDeliveryData DeliveryData = new ReadDeliveryData();
        List<Match_Data> matches = matchData.ReadFile();
        List<Delivery_Data> deliveries = DeliveryData.ReadDeliveryFile();
        String expectedResult = "{Gujarat Lions=132, Mumbai Indians=102, Sunrisers Hyderabad=124, Kings XI Punjab=83, Delhi Daredevils=109, Rising Pune Supergiants=101, Kolkata Knight Riders=130, Royal Challengers Bangalore=118}";

        Assert.assertNotNull(obj.extraruns(matches,deliveries).toString() );
        Assert.assertFalse(obj.extraruns(matches,deliveries).isEmpty());
        assertEquals(expectedResult, obj.extraruns(matches,deliveries).toString());

    }
}