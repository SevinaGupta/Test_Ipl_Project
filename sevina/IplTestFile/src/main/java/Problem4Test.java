import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class Problem4Test {

    @Test
    public void Problem4Test(){
        Problem4 obj = new Problem4();
        ReadMatchData matchData = new ReadMatchData();
        ReadDeliveryData DeliveryData = new ReadDeliveryData();
        List<Delivery_Data> deliveries = DeliveryData.ReadDeliveryFile();
        List<Match_Data> matches = matchData.ReadFile();
        String expectedResult = "[RN ten Doeschate=4.0, J Yadav=4.67, R Ashwin=5.85, S Nadeem=6.0, MC Henriques=6.27, Z Khan=6.32, MA Starc=6.8, Sandeep Sharma=6.96, GB Hogg=7.05, A Nehra=7.08]";

        Assert.assertNotNull(obj.findTopEconomicalBowlers(matches,deliveries).toString() );
        Assert.assertFalse(obj.findTopEconomicalBowlers(matches,deliveries).isEmpty());
        assertEquals(expectedResult, obj.findTopEconomicalBowlers(matches, deliveries).toString());


    }
}
