import java.util.HashMap;
import java.util.List;
import java.util.Map;


//    Display how many times the team that won the toss and won the match as well for all seasons?
public class WinMatchAndToss {

    public static Map wonMatchAndAlsoWonToss(List<Match_Data> matches) {
        Map<String,Integer> WonMatchAndToss = new HashMap<>();
        for(Match_Data match : matches){
            if(WonMatchAndToss.containsKey(match.getToss_winner()) && WonMatchAndToss.containsKey(match.getWinner())){
                   if(match.getToss_winner().equals(match.getWinner())){
                        int win = WonMatchAndToss.get(match.getWinner());
                        WonMatchAndToss.replace(match.getWinner(), win + 1);
                }
            }
            else{
                WonMatchAndToss.put(match.getWinner(), 0);
            }
        }
        return WonMatchAndToss;
    }

}
