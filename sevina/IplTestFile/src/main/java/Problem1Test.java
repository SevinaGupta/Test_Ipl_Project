import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class Problem1Test {

    @Test
    public void Problem1Test(){
        Problem1 obj = new Problem1();
        ReadMatchData matchData = new ReadMatchData();
        List<Match_Data> matches = matchData.ReadFile();
        String expectedResult = "{2009=57, 2008=58, 2017=59, 2016=60, 2015=59, 2014=60, 2013=76, 2012=74, season=1, 2011=73, 2010=60}";


        Assert.assertNotNull(obj.findNumberOfMatchesPerYear(matches).toString() );
        Assert.assertFalse(obj.findNumberOfMatchesPerYear(matches).isEmpty());
        Assert.assertEquals(expectedResult, obj.findNumberOfMatchesPerYear(matches).toString());

    }
}

