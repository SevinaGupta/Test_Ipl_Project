import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({Problem1Test.class,Problem2Test.class,Problem3Test.class,Problem4Test.class, WinMatchAndTossTest.class })

public class JUnit_TestSuite {
    @BeforeClass
    public static void printMe() {
        System.out.println("JUnitTestSuite is the test suite grouping And all test cases are running Successfully");

    }
}