import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ReadDeliveryData {

    private static final int match_id = 0;
    private static final int inning = 1;
    private static final int batting_team = 2;
    private static final int bowling_team = 3;
    private static final int over = 4;
    private static final int ball = 5;
    private static final int batsman = 6;
    private static final int non_striker = 7;
    private static final int bowler = 8;
    private static final int is_superover = 9;
    private static final int wide_runs = 10;
    private static final int bye_runs = 11;
    private static final int leg_bye_runs = 12;
    private static final int noball_runs = 13;
    private static final int penalty_runs = 14;
    private static final int batsman_runs = 15;
    private static final int extra_runs = 16;
    private static final int total_runs = 17;
    private static final int player_dissmissal = 18;
    private static final int dissmissal_kind = 19;
    private static final int fielder = 20;
    public List<Delivery_Data> ReadDeliveryFile(){

        List<Delivery_Data> deliveries = new ArrayList<>();


        try {
            FileReader fr = new FileReader("/home/crazyninja/sevina/IplTestFile/deliveries.csv");
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                Delivery_Data delivery = new Delivery_Data();

                //  Set All deliveries Data values through the setter method
                delivery.setMatch_id(data[match_id]);
                delivery.setInning(data[inning]);
                delivery.setBatting_team(data[batting_team]);
                delivery.setBowling_team(data[bowling_team]);
                delivery.setOver(data[over]);
                delivery.setBall(data[ball]);
                delivery.setBatsman(data[batsman]);
                delivery.setNon_striker(data[non_striker]);
                delivery.setBowler(data[bowler]);
                delivery.setIs_super_over(data[is_superover]);
                delivery.setWide_runs(data[wide_runs]);
                delivery.setBye_runs(data[bye_runs]);
                delivery.setLegbye_runs(data[leg_bye_runs]);
                delivery.setNoball_runs(data[noball_runs]);
                delivery.setPenalty_runs(data[penalty_runs]);
                delivery.setBatsman_runs(data[batsman_runs]);
                delivery.setExtra_runs(data[extra_runs]);
                delivery.setTotal_runs(data[total_runs]);

                deliveries.add(delivery);
            }
            fr.close();
            br.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return deliveries;
    }


}
