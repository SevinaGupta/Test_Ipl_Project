import java.util.List;

public class Main {
    public static void main(String[] args){
        Problem1 obj1 = new Problem1();
        Problem2 obj2 = new Problem2();
        Problem3 obj3 = new Problem3();
        Problem4 obj4 = new Problem4();
        WinMatchAndToss obj5 = new WinMatchAndToss();
        ReadMatchData matchData = new ReadMatchData();
        ReadDeliveryData DeliveryData = new ReadDeliveryData();
        List<Match_Data> matches = matchData.ReadFile();
        List<Delivery_Data> deliveries = DeliveryData.ReadDeliveryFile();

//        System.out.println(obj1.findNumberOfMatchesPerYear(matches));
//        System.out.println(obj2.findWonMatchesByEachTeam(matches));
//        System.out.println(obj3.extraruns(matches,deliveries));
//
//        List<Map.Entry<String, Double>> top10EconomyBowlers = obj4.findTopEconomicalBowlers(matches,deliveries);
//        for(int i=0; i<=9; i++) {
//            System.out.println(top10EconomyBowlers.get(i));
//        }
//        System.out.println(obj4.findTopEconomicalBowlers(matches,deliveries));

        System.out.println(obj5.wonMatchAndAlsoWonToss(matches));

    }
}
