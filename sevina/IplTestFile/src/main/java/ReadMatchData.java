import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReadMatchData {

    private static int id = 0;
    private static int season = 1;
    private static final int city = 2;
    private static final int date = 3;
    private static final int team1 = 4;
    private static final int team2 = 5;
    private static final int toss_winner = 6;
    private static final int toss_decision = 7;
    private static final int result = 8;
    private static final int dl_applied = 9;
    private static final int winner = 10;
    private static final int win_by_runs = 11;
    private static final int win_by_wickets = 12;
    private static final int player_of_match = 13;
    private static final int venue = 14;



    public static Map<String, Integer> problem1Result = new HashMap<>();

    public List<Match_Data> ReadFile(){


        List<Match_Data> matches = new ArrayList<>();


        try {
            FileReader fr = new FileReader("/home/crazyninja/sevina/IplTestFile/matches.csv");
            BufferedReader br = new BufferedReader(fr);
            String line = "";
            while ((line = br.readLine()) != null) {
                //System.out.print((String) line);
                String[] data = line.split(",");
                Match_Data match = new Match_Data();
                match.setId(data[id]);

                //  Set All Match Data values through the setter method
                match.setSeason(data[season]);
                match.setCity(data[city]);
                match.setDate(data[date]);
                match.setTeam1(data[team1]);
                match.setTeam2(data[team2]);
                match.setToss_winner(data[toss_winner]);
                match.setToss_decision(data[toss_decision]);
                match.setResult(data[result]);
                match.setDl_applied(data[dl_applied]);
                match.setWinner(data[winner]);
                match.setWin_by_runs(data[win_by_runs]);
                match.setWin_by_wickets(data[win_by_wickets]);
                matches.add(match);
            }
            br.close();
            fr.close();
        } catch (Exception e) {
            System.out.println(e);
        }




return matches;

    }
}

