import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class WinMatchAndTossTest {

    @Test
    public void WinMatchAndTossTest(){
        WinMatchAndToss obj = new WinMatchAndToss();
        ReadMatchData matchData = new ReadMatchData();
        List<Match_Data> matches = matchData.ReadFile();
        String expectedResult = "{=0, Mumbai Indians=47, Sunrisers Hyderabad=17, Pune Warriors=3, Rajasthan Royals=34, Kolkata Knight Riders=38, Royal Challengers Bangalore=25, Gujarat Lions=9, winner=0, Rising Pune Supergiant=4, Kochi Tuskers Kerala=3, Kings XI Punjab=27, Deccan Chargers=18, Delhi Daredevils=28, Rising Pune Supergiants=3, Chennai Super Kings=41}";

        Assert.assertNotNull(obj.wonMatchAndAlsoWonToss(matches).toString() );
        Assert.assertFalse(obj.wonMatchAndAlsoWonToss(matches).isEmpty());
        assertEquals(expectedResult, obj.wonMatchAndAlsoWonToss(matches).toString());

    }
}