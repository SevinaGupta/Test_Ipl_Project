import java.util.*;


//    4. For the year 2015 get the top economical bowlers
public class Problem4 {

    public static List findTopEconomicalBowlers(List<Match_Data> matches, List<Delivery_Data> deliveries){
        Map<String,Double> topEconomyOfBowlers = new HashMap<>();
        Map<String, TreeMap<String, Integer>> topEconomicalBowlersIn2015 = new TreeMap<>();
        List<String> matchesID=new ArrayList<String>();

        for (Match_Data match : matches) {
            if (match.getSeason().equals("2015")){
                matchesID.add(match.getId());
            }
        }
        for(Delivery_Data delivery : deliveries){
            if(matchesID.contains(delivery.getMatch_id())) {

                if(topEconomicalBowlersIn2015.containsKey(delivery.getBowler())){

                    int BallsCount = topEconomicalBowlersIn2015.get(delivery.getBowler()).get("balls") + 1;
                    int Runs = topEconomicalBowlersIn2015.get(delivery.getBowler()).get("runsConceded")+Integer.parseInt(delivery.getTotal_runs());
                    topEconomicalBowlersIn2015.get(delivery.getBowler()).replace("balls",BallsCount);
                    topEconomicalBowlersIn2015.get(delivery.getBowler()).replace("runsConceded",Runs);
                }
            else{
                    topEconomicalBowlersIn2015.put(delivery.getBowler(), new TreeMap<>());
                    topEconomicalBowlersIn2015.get(delivery.getBowler()).put("balls", 0);
                    topEconomicalBowlersIn2015.get(delivery.getBowler()).put("runsConceded", 0);
                }
            }
        }

        topEconomicalBowlersIn2015.forEach((key,value) -> {
            String bowlerName = key;
            double oversCount = value.get("balls")/6;
            double economyRate = value.get("runsConceded")/oversCount;
            String stringEconomyRate = String.format("%.2f",economyRate);
            topEconomyOfBowlers.put(bowlerName, Double.parseDouble(stringEconomyRate));
        });

        Set<Map.Entry<String, Double>> entrySet = topEconomyOfBowlers.entrySet();
//        TreeMap<Map.Entry<String, Integer>> treeSet = topEconomyOfBowlers.entrySet();
        List<Map.Entry<String, Double>> sortedlist = new ArrayList<>(entrySet);
        Collections.sort(sortedlist,(val1,val2) -> val1.getValue().compareTo(val2.getValue()));


//        System.out.println(topEconomicalBowlersIn2015);
//        System.out.println(topEconomyOfBowlers);
//        System.out.println(list);
        List<Map.Entry<String, Double>> top10Bowlerlist = new ArrayList<>();
        for(int i=0; i<=9; i++){
            top10Bowlerlist.add(sortedlist.get(i));
        }
//        System.out.println(top10Bowlerlist);
          return top10Bowlerlist;
    }

}
